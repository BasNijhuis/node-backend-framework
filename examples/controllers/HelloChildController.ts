import Controller from '@/controller';
import Route from '@/types/Route';
import { XResponse } from '@/xpress';

export default class HelloChildController extends Controller {
    path = '/child';
    routes: Route[] = [
        {
            path: '/',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello child' });
            }
        }
    ];
}
