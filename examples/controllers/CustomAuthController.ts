import Controller from '@/controller';
import Route from '@/types/Route';
import { XRequest, XResponse } from '@/xpress';

export default class CustomAuthController extends Controller {
    path = '/custom_auth';
    auth = true; // authorization required
    routes: Route[] = [
        {
            path: '/',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello world' });
            }
        }
    ];
    protected async authHandler(req: XRequest): Promise<boolean> {
        if (req.headers.authorization === 'token') return true;
        return false;
    }
}
