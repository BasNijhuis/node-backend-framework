import Controller from '@/controller';
import Route from '@/types/Route';
import { XResponse } from '@/xpress';

export default class HelloWorldController extends Controller {
    path = '/hello';
    routes: Route[] = [
        {
            path: '/',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello' });
            }
        },
        {
            path: '/world',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello world' });
            }
        },
        {
            path: '/other',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello other' });
            },
            roles: [4],
            roleKey: 'roleId'
        }
    ];
}
