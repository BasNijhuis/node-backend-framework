import Controller from '@/controller';
import Route from '@/types/Route';
import { XResponse, XRequest } from '@/xpress';

export default class CustomRouteAuthController extends Controller {
    path = '/custom_route_auth';
    // setting a authHandler on a route should set the route's auth to true
    routes: Route[] = [
        {
            path: '/',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello world' });
            },
            authHandler: async (req): Promise<boolean> => {
                if (req.headers.authorization === 'token2') return true;
                return false;
            }
        },
        {
            path: '/sub',
            requestHandler: async (_req, res): Promise<XResponse> => {
                return res.json({ message: 'Hello world' });
            }
        }
    ];
    // set to be sure it is executed when expected, and not when not
    protected async authHandler(req: XRequest): Promise<boolean> {
        if (req.headers.authorization === 'token') return true;
        return false;
    }
}
