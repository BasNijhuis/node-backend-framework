import App from '@/App';
import CronJob from '@/cronjob';
import calculateTask from '@e/tasks/calculate';

const app = new App([], [], [new CronJob('*/2 * * * * *', calculateTask)]);
app.start();
