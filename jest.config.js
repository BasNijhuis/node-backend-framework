module.exports = {
    globals: {
        'ts-jest': {
            compiler: 'ttypescript'
        }
    },
    testEnvironment: 'node',
    transform: {
        '.(ts|tsx)': require.resolve('ts-jest')
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    moduleNameMapper: {
        // Jest config for tsconfig.json's `compilerOptions.paths` property
        '^@/(.*)$': '<rootDir>/src/$1',
        '^@e/(.*)$': '<rootDir>/examples/$1',
        '^@t/(.*)$': '<rootDir>/__tests__/$1'
    }
};
