import { CronJob as Cron, CronCommand } from 'cron';
import moment from 'moment';

export default class CronJob {
    private readonly cron: Cron;

    constructor(
        schedule: string | Date | moment.Moment,
        task: CronCommand,
        onComplete?: CronCommand
    ) {
        this.cron = new Cron(
            schedule,
            task,
            onComplete,
            false,
            process.env.TIMEZONE
        );
    }
    /**
     * Start executing cron by schedule.
     */
    start(): void {
        this.cron.start();
    }

    /**
     * Stop executing cron.
     */
    stop(): void {
        this.cron.stop();
    }

    /**
     * @returns {boolean} true when running, else false.
     */
    get running(): boolean | undefined {
        return this.cron.running;
    }
}
