import MiddlewareError from './MiddlewareError';

export default interface SentryErrorHandlerOptions {
    shouldHandleError?(error: MiddlewareError): boolean;
}
