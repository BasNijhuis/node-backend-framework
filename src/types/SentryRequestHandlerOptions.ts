export default interface SentryRequestHandlerOptions {
    ip?: boolean;
    request?: boolean | string[];
    serverName?: boolean;
    transaction?: boolean | 'path' | 'methodPath' | 'handler';
    user?: boolean | string[];
    version?: boolean;
    flushTimeout?: number;
}
