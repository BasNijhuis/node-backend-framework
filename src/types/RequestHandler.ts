import { XRequest, XResponse } from '@/xpress';

/**
 * Handles a Route's specific request.
 */
type RequestHandler = (
    request: XRequest,
    response: XResponse
) => Promise<XResponse>;

export default RequestHandler;
