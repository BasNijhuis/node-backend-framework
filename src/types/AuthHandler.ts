import { XRequest } from '@/xpress';

/**
 * Handles authorization.
 * @returns {boolean} true if authorised, else false.
 */
type AuthHandler = (
    request: XRequest,
    roleKey: string,
    roles?: readonly any[]
) => Promise<boolean>;

export default AuthHandler;
