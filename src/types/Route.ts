import AuthHandler from './AuthHandler';
import Method from './Method';
import RequestHandler from './RequestHandler';
import Role from './Role';

export default interface Route {
    /**
     * The path of the route.
     */
    readonly path?: string;

    /**
     * The function which will handle the base request.
     */
    readonly requestHandler: RequestHandler;

    /**
     * When set to true, runs the auth handler.
     * When an authHandler is defined within a Route,
     * it will be used, otherwise the controller's auth handler will be used.
     */
    readonly auth?: boolean;

    /**
     * The HTTP method to use.
     * When left undefined, it uses all methods.
     */
    readonly method?: Method;

    /**
     * The AuthHandler for this specific route.
     */
    readonly authHandler?: AuthHandler;

    /**
     * The roles which may access this route when `auth` is explicitly set to true.
     */
    readonly roles?: readonly Role[];

    /**
     * The key of the role in the decoded JWT. Defaults to controller's roleKey.
     */
    readonly roleKey?: string;
}
