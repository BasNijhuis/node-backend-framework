/**
 * A role can either be a string or number as value or object.
 * @param allowHigher When explicitly set to true, allows roles higher than this instance.
 * @param allowLower When explicitly set to true, allows roles lower than this instance.
 */
type Role =
    | number
    | string
    | { value: number | string; allowHigher?: boolean; allowLower?: boolean };

export default Role;
