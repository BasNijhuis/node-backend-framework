import { Request, Response, NextFunction } from 'express';
import HttpException from '@/exceptions/HttpException';

export default (
    error: HttpException,
    _req: Request,
    res: Response,
    next: NextFunction
): void => {
    console.error(error);
    const status = error.status || 500;
    const message =
        error.message && status < 500
            ? error.message
            : 'Something went wrong, try again later.';
    res.status(status).json({
        message
    });
    next();
};
