import http from 'http';
import express from 'express';
import dotenv from 'dotenv';
import * as Sentry from '@sentry/node';

dotenv.config(); // read .env

import cors from './config/cors';
import Controller from './controller';
import errorHandler from './middleware/errorHandler';
import { Server } from 'http';
import CronJob from './cronjob';
import SentryRequestHandlerOptions from './types/SentryRequestHandlerOptions';
import SentryErrorHandlerOptions from './types/SentryErrorHandlerOptions';

export default class App {
    readonly express: express.Application; // express Application instance
    private _cronJobsStarted = false;
    private server?: Server;
    private _sentryEnabled = false;

    constructor(
        public readonly controllers?: readonly Controller[],
        private middlewares?: any[],
        private _cronJobs?: CronJob[],
        sentryInitOptions?: Sentry.NodeOptions,
        sentryRequestHandlerOptions?: SentryRequestHandlerOptions,
        sentryErrorHandlerOptions?: SentryErrorHandlerOptions,
        private _port = process.env.PORT ? Number(process.env.PORT) : 8080
    ) {
        this.express = express();
        this.initSentry(sentryInitOptions, sentryRequestHandlerOptions);
        this.initMiddleware();
        this.useControllers();
        this.initErrorHandler(sentryErrorHandlerOptions);
    }

    get port(): number {
        return this._port;
    }

    get cronJobs(): CronJob[] | undefined {
        return this._cronJobs;
    }

    get cronJobsStarted(): boolean {
        return this._cronJobsStarted;
    }

    get sentryEnabled(): boolean {
        return this._sentryEnabled;
    }

    /**
     * Initializes Sentry and its request handler.
     * @param initOptions The Sentry init options.
     */
    private initSentry(
        initOptions: Sentry.NodeOptions | undefined,
        requestHandlerOptions: SentryRequestHandlerOptions | undefined
    ): void {
        if (!initOptions) initOptions = {};
        if (!initOptions.dsn)
            if (process.env.SENTRY_URL)
                initOptions.dsn = process.env.SENTRY_URL;
            else return;
        Sentry.init(initOptions);
        this.express.use(Sentry.Handlers.requestHandler(requestHandlerOptions));
        this._sentryEnabled = true;
    }

    /**
     * Initializes middleware to be used.
     */
    private initMiddleware(): void {
        const corsConfig = cors();
        if (corsConfig !== null) this.express.use(corsConfig);
        if (this.middlewares !== undefined)
            for (const middleware of this.middlewares) {
                if (typeof middleware === 'object' && middleware.mPath)
                    this.express.use(middleware.mPath, middleware.middleware);
                else this.express.use(middleware);
            }
    }

    /**
     * Start using all the controllers' routers.
     * @param controllers Array of controllers to be used.
     * @returns {boolean} true when controllers are being used after calling this method, else false.
     */
    private useControllers(): boolean {
        if (!this.controllers) return false;
        const usingControllers: string[] = [];
        for (const controller of this.controllers) {
            if (usingControllers.includes(controller.constructor.name))
                throw Error(
                    `Duplicate controller ${controller.constructor.name} in application instance.`
                );
            controller.use(this.express);
            usingControllers.push(controller.constructor.name);
        }
        return true;
    }

    /**
     * Starts the cron jobs.
     * @returns {boolean} true if cron jobs were started by this method, else false.
     */
    startCronJobs(): boolean {
        if (!this.cronJobs || this.cronJobsStarted) return false;
        for (const cronJob of this.cronJobs) {
            cronJob.start();
        }
        this._cronJobsStarted = true;
        console.log('Cron jobs started');
        return true;
    }

    /**
     * Starts the Express server.
     * @param message Optional custom message which is logged after a succesful start up.
     * @returns {boolean} true when server was started by this method, else false.
     */
    startExpressServer(message?: string): boolean {
        if (!this.controllers) return false;
        if (this.server) {
            console.warn('Server already started, ignoring');
            return false;
        }
        message = message || `Backend server started at port ${this.port}`;
        this.server = http.createServer(this.express);
        if (process.env.NODE_ENV !== 'test')
            this.server.listen(this.port, () => {
                console.log(message);
            });
        else console.log(message);
        return true;
    }

    /**
     * Stops the cron jobs.
     * @returns {boolean} true if the cron jobs were stopped by calling this method, else false.
     */
    stopCronJobs(): boolean {
        if (!this.cronJobs || !this.cronJobsStarted) return false;
        for (const cronJob of this.cronJobs) {
            cronJob.stop();
        }
        this._cronJobsStarted = false;
        console.log('Cron jobs stopped');
        return true;
    }

    /**
     * Stops the Express server.
     * @returns {boolean} true if server was stopped by calling this method, else false.
     */
    stopExpressServer(): boolean {
        if (this.server === undefined) return false;
        this.server.close();
        console.log('HTTP server closed');
        return true;
    }

    /**
     * Initialize error handler and Sentry error handler if Sentry is enabled.
     */
    private initErrorHandler(
        options: SentryErrorHandlerOptions | undefined
    ): void {
        if (this.sentryEnabled)
            this.express.use(Sentry.Handlers.errorHandler(options));
        this.express.use(errorHandler);
    }

    /**
     * Starts the application (Express server + cron jobs).
     * @param message The message to log on successful server start up.
     * @returns {boolean} true when anything was started by this method, else false.
     */
    start(message?: string): boolean {
        const cronJobsStarted = this.startCronJobs();
        const serverStarted = this.startExpressServer(message);
        if (!cronJobsStarted && !serverStarted) {
            console.warn(
                'Nothing to start, make sure you have any controller or cron job set.'
            );
            return false;
        }
        return true;
    }

    /**
     * Stops the application (Express server + cron jobs).
     * @param message The message to log on successful shut down.
     * @returns {boolean} true when anything was stopped by this method, else false.
     */
    stop(message?: string): boolean {
        const cronJobsStopped = this.stopCronJobs();
        const serverStopped = this.stopExpressServer();
        if (!cronJobsStopped && !serverStopped) {
            console.warn('Nothing stopped, since nothing was running.');
            return false;
        }
        console.log(
            message || `Backend server which ran at port ${this.port} stopped`
        );
        return true;
    }
}
