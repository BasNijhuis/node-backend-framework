import { Request, Response } from 'express';

export declare interface XRequest extends Request {
    decodedJwt?: Record<string, unknown> | string;
}

export declare type XResponse = Response;
