import express from 'express';
import jwt from 'jsonwebtoken';
import * as Sentry from '@sentry/node';

import { XRequest, XResponse } from '@/xpress';
import Method from '@/types/Method';
import RequestHandler from '@/types/RequestHandler';
import Role from '@/types/Role';
import Route from '@/types/Route';

/**
 * Abstract base controller.
 * Extend it and configure routes.
 */
export default abstract class Controller {
    abstract readonly path: string; // implemented by child controllers
    abstract readonly routes: readonly Route[];
    auth?: boolean;
    roles?: readonly Role[]; // the roles which may access this controller when `auth` is true. allows all valid JWTs when not set.
    roleKey = 'role'; // default key of the decoded JWT with the Role value, overwrite with child.
    private readonly router: express.Router;
    private using = false;

    constructor(public readonly parent?: Controller) {
        this.router = express.Router();
    }

    /**
     * Returns the full path
     */
    get fullPath(): string {
        return `${this.parent ? this.parent.fullPath : ''}${this.path}`;
    }

    /**
     * Initializes the routes.
     */
    private initRoutes(): void {
        for (const route of this.routes) {
            const auth = route.auth !== undefined ? route.auth : this.auth;
            const routePath = route.path || '';
            if (auth === false && route.authHandler)
                console.warn(`The \`auth\` property of the route with
                 path \`${this.fullPath}${routePath}\` or its controller
                 \`${this.constructor.name}\` is explicitly set to false,
                 but an \`authHandler\` is set for the route. The \`authHandler\` will NOT be executed!`);
            const routeTuple: readonly [string, RequestHandler] = [
                routePath,
                async (
                    request: XRequest,
                    response: XResponse
                ): Promise<XResponse> =>
                    await this.handleRequest(route, request, response)
            ];
            switch (route.method) {
                case undefined:
                    this.router.all(...routeTuple);
                    break;
                case Method.GET:
                    this.router.get(...routeTuple);
                    break;
                case Method.HEAD:
                    this.router.head(...routeTuple);
                    break;
                case Method.POST:
                    this.router.post(...routeTuple);
                    break;
                case Method.PUT:
                    this.router.put(...routeTuple);
                    break;
                case Method.DELETE:
                    this.router.delete(...routeTuple);
                    break;
                case Method.CONNECT:
                    this.router.connect(...routeTuple);
                    break;
                case Method.OPTIONS:
                    this.router.options(...routeTuple);
                    break;
                case Method.TRACE:
                    this.router.trace(...routeTuple);
                    break;
                default:
                    throw Error(`Unsupported HTTP method ${route.method}.`);
            }
        }
    }

    /**
     * Use the controller's router.
     * @param express The express application instance.
     */
    use(express: express.Application): void {
        if (this.using) throw Error(`Already using ${this.constructor.name}.`);
        this.initRoutes();
        express.use(this.fullPath, this.router);
        this.using = true;
    }

    /**
     * Returns default internal server error response.
     * @param response Express response object.
     * @returns {XResponse} Internal server error as Express JSON response.
     */
    protected serverErrorResponse(response: XResponse): XResponse {
        return response
            .status(500)
            .json({ message: 'Something went wrong, try again later.' });
    }

    /**
     * The default AuthHandler to run when `auth` of the controller or route is set to true.
     * This method may be overridden by the child controllers
     * to allow different auth handling between controllers.
     * By default, it checks for a Bearer JWT in the Authorization header and tries to verify it.
     * If successful, the decodedJwt will be injected into the Request object and the method will return true, else false.
     * @param request Express request object.
     * @param roleKey The key of the decoded JWT which has a role value.
     * @param roles The roles to allow.
     * @returns true if authorised, else false.
     */
    protected async authHandler(
        request: XRequest,
        roleKey: string,
        roles?: readonly Role[]
    ): Promise<boolean> {
        if (!process.env.JWT_SECRET)
            throw Error('No JWT_SECRET environment variable set.');
        const authHeader = request.headers.authorization;
        if (authHeader === undefined) return false;
        if (!authHeader.startsWith('Bearer ')) return false;
        const token = authHeader.replace(/^(Bearer )/, '');
        try {
            const decoded: any = jwt.verify(token, process.env.JWT_SECRET);
            if (decoded instanceof String) return false;
            if (
                roles &&
                (decoded[roleKey] === undefined ||
                    !roles.find((role) => {
                        if (role instanceof Object) {
                            if (role.allowHigher) {
                                return decoded[roleKey] >= role.value;
                            }
                            if (role.allowLower) {
                                return decoded[roleKey] <= role.value;
                            }
                        }
                        return decoded[roleKey] === role;
                    }))
            )
                return false;
            request.decodedJwt = decoded;
            return true;
        } catch {
            // ignored
        }
        return false;
    }

    /**
     * Handles all of the controller's requests.
     * @param route The route for which the request is being handled.
     * @param request Express Request object.
     * @param response Express Response object.
     * @returns {XResponse} Express Response.
     */
    private async handleRequest(
        route: Route,
        request: XRequest,
        response: XResponse
    ): Promise<XResponse> {
        try {
            // handle authentication if set
            const auth = route.auth !== undefined ? route.auth : this.auth;
            const roleKey =
                route.roleKey !== undefined ? route.roleKey : this.roleKey;
            const roles = route.roles !== undefined ? route.roles : this.roles;
            if (auth || (auth !== false && route.authHandler)) {
                const authHandler =
                    route.authHandler !== undefined
                        ? route.authHandler
                        : this.authHandler; // use route's authHandler if set, else the controller's
                if (!(await authHandler(request, roleKey, roles)))
                    return response.status(401).json({
                        message: "You're not authorised to access this path."
                    });
            }
            return await route.requestHandler(request, response); // run the route's specific request handler
        } catch (e) {
            if (process.env.NODE_ENV !== 'production') console.error(e);
            else if (process.env.SENTRY_URL) Sentry.captureException(e);
        }
        return this.serverErrorResponse(response);
    }
}
