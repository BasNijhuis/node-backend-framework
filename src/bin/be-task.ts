#!/usr/bin/env node

import fs from 'fs';
import dotenv from 'dotenv';
import chalk from 'chalk';
import figlet from 'figlet';
import program from 'commander';
import * as Sentry from '@sentry/node';
import { CaptureConsole } from '@sentry/integrations';

dotenv.config(); // inject .env into process.env

const defaultDefaultTaskDir = 'dist/tasks'; // when undefined env var TASK_DIR
const defaultTaskDir = process.env.TASK_DIR || defaultDefaultTaskDir;
const appRoot = process.cwd();

let code = 0; // 0 = success, anything else = error, the code represents the amount of errors

/**
 * Prints the task directory.
 */
function printTaskDir(): void {
    console.log(`Task directory: "${program.dir}"`);
}

/**
 * Print error and increment code.
 */
function handleError(e: string | Error): void {
    console.error(e);
    code++;
}

/**
 * @returns {number} code
 */
export function getCode(): number {
    return code;
}

/**
 * Resets code back to 0.
 */
export function resetCode(): void {
    code = 0;
}

/**
 * Runs all tasks.
 */
export async function runAll(): Promise<void> {
    console.log('Running all tasks from task directory');
    try {
        const files = fs
            .readdirSync(`${appRoot}/${program.dir}`)
            .filter((file) => file.endsWith('.js'));
        await Promise.all(
            // async foreach
            files.map(async (file) => {
                const {
                    default: task
                }: { default: () => Promise<void> } = await import(
                    `${appRoot}/${program.dir}/${file}`
                );
                await task();
            })
        );
    } catch (e) {
        handleError(e);
    }
}

/**
 * Runs a single task.
 */
export async function runSingle(name: string): Promise<void> {
    console.log(`Running "${name}" from task directory`);
    try {
        const {
            default: task
        }: { default: () => Promise<void> } = await import(
            `${appRoot}/${program.dir}/${name}`
        );
        await task();
    } catch (e) {
        handleError(e);
    }
}

if (require.main === module) {
    // if not imported (directly called from CLI)

    if (process.env.SENTRY_URL)
        Sentry.init({
            dsn: process.env.SENTRY_URL,
            integrations: [
                new CaptureConsole({
                    levels: ['error']
                })
            ]
        });

    program
        .version('1.2.0')
        .allowUnknownOption()
        .option('-T, --task <file name>', 'runs a single task by its name')
        .option('-A, --all', 'runs all tasks')
        .option(
            '-d, --dir <directory>',
            `sets the task directory (default set by environment variable "TASK_DIR", when undefined: "${defaultDefaultTaskDir}")`,
            defaultTaskDir
        )
        .parse(process.argv);
    console.log(
        chalk.red(figlet.textSync('TASK_RUNNER', { horizontalLayout: 'full' }))
    );
    printTaskDir();
    (async (): Promise<void> => {
        if (program.all) {
            await runAll();
        } else if (program.task) {
            await runSingle(program.task);
        } else {
            program.help();
        }
        process.exit(code);
    })();
} else {
    program.dir = defaultTaskDir; // set default directory when imported for testing
}
