#!/usr/bin/env node

import { spawn } from 'child_process';
import path from 'path';
import fse from 'fs-extra';
import dotenv from 'dotenv';
import chalk from 'chalk';
import figlet from 'figlet';
import inquirer from 'inquirer';

dotenv.config(); // inject .env into process.env

const workDir = process.cwd();

let code = 0; // 0 = success, anything else = error, the code represents the amount of errors

/**
 * Print error and increment code.
 */
function handleError(e: string | Error): void {
    console.error(e);
    code++;
}

/**
 * Asks a question and can be awaited for user input.
 * @param query The question text.
 * @param choices The choices to answer with, default: Yes or No.
 * @return {string} Use input.
 */
async function askQuestion(
    query: string,
    choices = ['Yes', 'No']
): Promise<string> {
    const ans = await inquirer.prompt({
        name: 'answer',
        message: query,
        type: 'list',
        choices
    });
    return ans.answer;
}

/**
 * @returns {number} code
 */
export function getCode(): number {
    return code;
}

/**
 * Resets code back to 0.
 */
export function resetCode(): void {
    code = 0;
}

/**
 * Installs the node modules.
 * @param packageManager Default is yarn.
 */
async function execModuleInstall(packageManager = 'yarn'): Promise<void> {
    console.log(`Running '${packageManager} install'...`);
    const runningCommand = spawn(packageManager, ['install']);

    runningCommand.on('error', async function(err) {
        console.error(err);
    });

    runningCommand.on('exit', function(cCode: number) {
        code += cCode;
    });

    for await (const data of runningCommand.stdout) {
        console.log(Buffer.from(data as Buffer).toString('utf-8'));
    }
    if (code === 0) console.log('Project successfully initialized!');
}

/**
 * Initializes a project in the current working directory.
 */
export async function init(): Promise<void> {
    try {
        const packageManager = (
            await askQuestion('Which package manager will you be using?', [
                'Yarn',
                'NPM'
            ])
        ).toLowerCase();
        console.log('Copying boilerplate code...');
        const boilerplatePath = path.resolve(__dirname, '../../boilerplate');
        await fse.copy(boilerplatePath, workDir);
        await fse.copyFile(
            `${boilerplatePath}/.env.example`,
            `${workDir}/.env`
        );
        console.log('Boilerplate code successfully copied!');
        process.chdir(workDir);
        await execModuleInstall(packageManager);
    } catch (e) {
        handleError(e);
    }
}

if (require.main === module) {
    // if not imported (directly called from CLI)
    console.log(
        chalk.red(figlet.textSync('INIT_PROJECT', { horizontalLayout: 'full' }))
    );
    (async (): Promise<void> => {
        const answer = await askQuestion(
            'Everything in the current working directory will be overwritten! Continue?'
        );
        if (answer === 'Yes') await init();
        else if (answer === 'No') {
            console.log('Init project aborted.');
            return;
        } else {
            handleError('Unexpected answer to confirmation.');
        }
        process.exit(code);
    })();
}
