import express from 'express';
import cors, { CorsOptions } from 'cors';

/**
 * Sets up CORS configuration.
 */
export default (): express.RequestHandler => {
    if (process.env.CORS_WHITELIST === '*') return cors();
    const corsWhitelist = JSON.parse(process.env.CORS_WHITELIST || '[]');
    const corsOptions: CorsOptions = {
        origin: (origin, callback) => {
            if (!origin || corsWhitelist.indexOf(origin) !== -1) {
                callback(null, true);
            } else {
                callback(
                    process.env.CORS_ERROR
                        ? new Error(process.env.CORS_ERROR)
                        : null,
                    false
                );
            }
        },
        optionsSuccessStatus: 200 // to support IE11, actually 204
    };

    return cors(corsOptions);
};
