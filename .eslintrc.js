module.exports = {
    root: true,

    // Rules order is important, please avoid shuffling them
    extends: [
        // Base ESLint recommended rules
        'eslint:recommended',

        // ESLint typescript rules
        // See https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#usage
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',

        // Usage with Prettier, provided by 'eslint-config-prettier'.
        // https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#usage-with-prettier
        'prettier',
        'prettier/@typescript-eslint'
    ],

    plugins: [
        // Required to apply rules which need type information
        '@typescript-eslint'
        // Prettier has not been included as plugin to avoid performance impact
        // See https://github.com/typescript-eslint/typescript-eslint/issues/389#issuecomment-509292674
        // Add it as an extension
    ],

    parserOptions: {
        parser: '@typescript-eslint/parser',
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        },
        project: 'tsconfig.json'
    },

    env: {
        amd: true,
        node: true
    },

    globals: {
        process: true
    },

    ignorePatterns: ['lib', 'boilerplate', '.eslintrc.js'],

    // add your custom rules here
    rules: {
        quotes: ['error', 'single', { avoidEscape: true }],
        camelcase: 'off', // Must be disabled since incorrect errors may be reported
        'prefer-promise-reject-errors': 'off',
        '@typescript-eslint/indent': ['error', 4],
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/naming-convention': [
            'error',
            { selector: 'enumMember', format: ['UPPER_CASE'] }
        ], // Force camelcase
        '@typescript-eslint/no-unused-vars': 'error',
        '@typescript-eslint/no-explicit-any': 'off',
        'no-console': 'off', // allow console
        'no-debugger': 'off' // allow debugger
    }
};
