import { runAll, getCode, runSingle, resetCode } from '@/bin/be-task';

describe('task runner tests', () => {
    afterEach(() => {
        resetCode();
    });
    test('should run all tasks', async () => {
        await runAll();
        expect(getCode()).toEqual(0);
    });
    test('should run existing tasks', async () => {
        await runSingle('hello');
        await runSingle('calculate');
        expect(getCode()).toEqual(0);
    });
    test('should error on non-existing task', async () => {
        await runSingle('hello1');
        expect(getCode()).toEqual(1);
    });
    test('code should represent amount of errors', async () => {
        await runSingle('hello1');
        await runSingle('hello2');
        expect(getCode()).toEqual(2);
    });
});
