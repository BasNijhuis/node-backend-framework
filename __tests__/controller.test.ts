import request from 'supertest';
import jwt from 'jsonwebtoken';

import App from '@/.';
import HelloWorldController from '@e/controllers/HelloWorldController';
import HelloChildController from '@e/controllers/HelloChildController';
import CustomAuthController from '@e/controllers/CustomAuthController';
import CustomRouteAuthController from '@e/controllers/CustomRouteAuthController';

describe('controller tests', () => {
    let app: App | undefined;
    // use example classes for testing
    beforeEach(() => {
        // start clean app for each test
        const helloWorldController = new HelloWorldController();
        app = new App([
            helloWorldController,
            new HelloChildController(helloWorldController),
            new CustomAuthController(),
            new CustomRouteAuthController()
        ]);
        app.start();
    });

    afterEach(() => {
        if (app === undefined) fail('App undefined');
        app.stop();
        app = undefined;
    });

    // HelloWorldController tests begin
    test('call route without any authorization, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const res = await request(app.express).get('/hello');

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with sub path, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const res = await request(app.express).get('/hello/world');

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token without roles, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        const token = jwt.sign(
            { test: 'hello world' },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with role, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [1]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 1 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with allow higher role, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [{ value: 1, allowHigher: true }]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 2 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with allow lower role, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [{ value: 2, allowLower: true }]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 1 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization without token, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true;
        const res = await request(app.express).get('/hello');

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with invalid role, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [1]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 2 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with allow higher role but send lower, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [{ value: 2, allowHigher: true }]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 1 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with allow lower role but send higher, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [{ value: 2, allowLower: true }]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 3 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with route overriding roles and roleKey and sending route role, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [2]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', roleId: 4 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello/other')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with default authorization with token with route overriding roles and roleKey and sending controller role, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        app.controllers[0].auth = true; // enable authorization being required for this controller
        app.controllers[0].roles = [2]; // set role which may access this
        const token = jwt.sign(
            { test: 'hello world', role: 2 },
            process.env.JWT_SECRET as string
        );
        const req = request(app.express)
            .get('/hello/other')
            .set('Authorization', `Bearer ${token}`);
        const res = await req;

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    // HelloWorldController tests end

    // CustomAuthController tests begin
    test('call route with custom controller authorization with token, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const req = request(app.express)
            .get('/custom_auth')
            .set('Authorization', 'token');
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with custom controller authorization without token, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const res = await request(app.express).get('/custom_auth');

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    // CustomAuthController tests end

    // CustomRouteAuthController tests begin
    test('call route with custom route authorization with token, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const req = request(app.express)
            .get('/custom_route_auth')
            .set('Authorization', 'token2');
        const res = await req;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    test('call route with custom route authorization without token, expect 401', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const res = await request(app.express).get('/custom_route_auth');

        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('message');
    });
    test('call route without auth in same controller as a route with auth, expect 200', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const res = await request(app.express).get('/custom_route_auth/sub');

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    // CustomRouteAuthController tests end
    // HelloChildController tests begin
    test('child controller using parent path', async () => {
        if (app === undefined) fail('App undefined');
        if (app.controllers === undefined) fail('Controllers undefined');
        const res = await request(app.express).get('/hello/child');
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });
    // HelloChildController tests end
});
