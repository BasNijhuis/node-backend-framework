import {
    Controller,
    Method,
    Route,
    XRequest,
    XResponse
} from 'node-backend-framework';

/**
 * Controller regarding Hello world routes.
 */
export default class HelloWorldController extends Controller {
    readonly path = 'hello'; // no '/' since parent path ends with it (see /src/index.ts), will else result in '//hello'
    readonly routes: readonly Route[] = [
        {
            path: '/world', // the subpath, will result in => /hello/world
            requestHandler: this.helloWorld, // the function which will handle the request
            method: Method.GET // HTTP method
        }
    ]; // obligated override, set the controller's routes

    /**
     * Return JSON with Hello world message.
     * @param _req Express request object.
     * @param res Express response object.
     * @this undefined
     * @returns Express JSON response with Hello world message.
     */
    private async helloWorld(
        _req: XRequest, // prepended with _ since it remains unused (linter)
        res: XResponse
    ): Promise<XResponse> {
        return res.json({ message: 'Hello world' });
    }
}
