import {
    Controller,
    Method,
    Route,
    XRequest,
    XResponse
} from 'node-backend-framework';

/**
 * Controller regarding Hello world routes.
 */
export default class RootController extends Controller {
    readonly path = '/'; // obligated override, set the base path for this controller
    readonly routes: readonly Route[] = [
        {
            requestHandler: this.root, // the function which will handle the request
            method: Method.GET // HTTP method
        }
    ]; // obligated override, set the controller's routes

    /**
     * Return JSON with Hello world message.
     * @param _req Express request object.
     * @param res Express response object.
     * @this undefined
     * @returns Express JSON response with message to visit /hello/world.
     */
    private async root(
        _req: XRequest, // prepended with _ since it remains unused (linter)
        res: XResponse
    ): Promise<XResponse> {
        return res.json({ message: 'Visit /hello/world' });
    }
}
