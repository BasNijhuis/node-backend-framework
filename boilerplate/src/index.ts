import App, { CronJob } from 'node-backend-framework'; // make sure to import first so .env is injected from the start
import express from 'express';
import HelloWorldController from './controllers/HelloWorldController';
import helloWorldTask from './tasks/hello';
import RootController from './controllers/RootController';

const rootController = new RootController();
const helloWorldController = new HelloWorldController(rootController); // rootController as parent, just as an example
const app = new App(
    [rootController, helloWorldController], // controllers
    [express.json()], // optional, Express middleware to be used
    [new CronJob('*/10 * * * * *', helloWorldTask)] // optional cron jobs
);

app.start(); // start

export default app;
