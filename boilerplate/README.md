# Node Backend application

Examples shown are done with `yarn`. Commands can also be executed using `npm`.

## Starting the server

Start server normally:

```
yarn start
```

Start server which watches files and hot-reloads using [nodemon](https://yarn.pm/nodemon):

```
yarn start:watch
```

## Linting the source files

Lint the source files using [ESLint](https://yarn.pm/eslint):

```
yarn lint
```

## Formatting the source files

Format the source files using [Prettier](https://yarn.pm/prettier):

```
yarn format
```

## Running the tasks

The source code of the tasks are found in `src/tasks`, which results in the compiled folder `dist/tasks`. The compiled folder can be set to anywhere you like with environment variable `TASK_DIR`.

Run all tasks:

```bash
yarn task -A # or --all
```

Run single task (example from boilerplate):

```bash
yarn task -T hello # or --task
```

To run the tasks without auto-compiling (it has to be compiled earlier), replace `yarn task` with `npx be-task`
